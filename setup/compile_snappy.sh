SH_SNAPPY_DIR="./workstation/snap/"
SH_SNAPPY_FILE="esa-snap_all_unix_8_0.sh"
IN_COMMANDS="./in_commands"
PYTHON_EXE="/usr/bin/python3.9"

cd "$SH_SNAPPY_DIR"
echo "Compile esa-snap_all_unix_8_0.sh"
sh "$SH_SNAPPY_FILE" < "$IN_COMMANDS"

cd "/opt/snap/bin/"
if ./snappy-conf "$PYTHON_EXE"; then
  echo "Process finished successfully!"
else
  mv "/installation_esa_snappy/jpy-0.9.0-cp39-cp39-linux_x86_64.whl" "/root/.snap/snap-python/snappy/"
  ./snappy-conf "$PYTHON_EXE"
  echo "Process finished successfully!"
fi

