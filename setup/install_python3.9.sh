# Add Python repository
add-apt-repository ppa:deadsnakes/ppa -y
# Install python
apt-get update && apt-get install python3.9 python3.9-dev python3.9-distutils gcc curl -y
alias python='python3.9'
# Install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
