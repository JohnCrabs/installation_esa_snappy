# Extract the JDK move it to /opt/ and add the Environment Variables to /root/.profile
MAVEN_DIR="apache-maven-3.8.5"
if [ -d MAVEN_DIR ]; then
        echo "$MAVEN_DIR exists!"
else
        tar -xvf "./workstation/maven/apache-maven-3.8.5-bin.tar.gz"
fi

# Check if JDK exists in /opt/ and remove it
M2_HOME="/opt/$MAVEN_DIR"
if [ -d "$M2_HOME" ]; then
        echo "Remove $M2_HOME"
        rm -r "$M2_HOME"
fi

# Move jdk folder to final location
echo "Move $MAVEN_DIR to $M2_HOME"
mv "$MAVEN_DIR" /opt/

# Add Environment Variables to root/.profile
PROFILE="./workstation/maven/profile"
echo "Add Environment Variables to $PROFILE"
echo "" > "$PROFILE"
echo "M2_HOME=\"$M2_HOME\"" >> "$PROFILE"
echo "PATH=\"\$M2_HOME/bin:\$PATH\"" >> "$PROFILE"
echo "export PATH" >> "$PROFILE"

# source
if source "$PROFILE"; then
  echo "source \"$PROFILE\""
else
  . "$PROFILE"
  echo ". \"$PROFILE\""
fi
mvn -version
