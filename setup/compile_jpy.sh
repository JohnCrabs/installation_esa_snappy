echo "Unpacking jpy-0.9.0.tar.gz"
tar -xvf "./workstation/jpy/jpy-0.9.0.tar.gz" -C "./workstation/jpy"
cd "./workstation/jpy/jpy-0.9.0/"
echo "Execute setup.py"
python3.9 "./setup.py" build maven bdist_wheel
echo "Compiling jpy finished successfully"
cd "../../.."
cp "./workstation/jpy/jpy-0.9.0/dist/jpy-0.9.0-cp39-cp39-linux_x86_64.whl" "./"