# Extract the JDK move it to /opt/ and add the Environment Variables to /root/.profile
JDK_DIR="jdk-20.0.1"
if [ -d JDK_DIR ]; then
        echo "$JDK_DIR exists!"
else
        tar -xvf "./workstation/jdk/openjdk-20.0.1_linux-x64_bin.tar.gz"
fi

# Check if JDK exists in /opt/ and remove it
JDK_HOME="/opt/$JDK_DIR"
if [ -d "$JDK_HOME" ]; then
        echo "Remove $JDK_HOME"
        rm -r "$JDK_HOME"
fi

# Move jdk folder to final location
echo "Move jdk-20.0.1 to $JDK_HOME"
mv jdk-20.0.1 /opt/

# Add Environment Variables to root/.profile
PROFILE="./workstation/jdk/profile"
echo "Add Environment Variables to $PROFILE"
echo "" > "$PROFILE"
echo "JAVA_HOME=\"$JDK_HOME\"" >> "$PROFILE"
echo "PATH=\"\$JAVA_HOME/bin:\$PATH\"" >> "$PROFILE"
echo "JDK_HOME=\"\$JAVA_HOME\"" >> "$PROFILE"
echo "export PATH" >> "$PROFILE"
echo "export JAVA_HOME" >> "$PROFILE"
echo "export JDK_HOME" >> "$PROFILE"

# source
if source "$PROFILE"; then
  echo "source \"$PROFILE\""
else
  . "$PROFILE"
  echo ". \"$PROFILE\""
fi

java -version
