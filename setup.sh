SH_UBUNTU_UTILS="./setup/install_ubuntu_utils.sh"
SH_PYTHON="./setup/install_python3.9.sh"
SH_JDK="./setup/setup_jdk.sh"
SH_MAVEN="./setup/setup_maven.sh"
SH_JPY="./setup/compile_jpy.sh"
SH_SNAPPY="./setup/compile_snappy.sh"

if source "$SH_UBUNTU_UTILS"; then
  echo "source \"$SH_UBUNTU_UTILS\""
else
  . "$SH_UBUNTU_UTILS"
  echo ". \"$SH_UBUNTU_UTILS\""
fi

if source "$SH_PYTHON"; then
  echo "source \"$SH_PYTHON\""
else
  . "$SH_PYTHON"
  echo ". \"$SH_PYTHON\""
fi

if source "$SH_JDK"; then
  echo "source \"$SH_JDK\""
else
  . "$SH_JDK"
  echo ". \"$SH_JDK\""
fi

if source "$SH_MAVEN"; then
  echo "source \"$SH_MAVEN\""
else
  . "$SH_MAVEN"
  echo ". \"$SH_MAVEN\""
fi

if source "$SH_JPY"; then
  echo "source \"$SH_JPY\""
else
  . "$SH_JPY"
  echo ". \"$SH_JPY\""
fi

if source "$SH_SNAPPY"; then
  echo "source \"$SH_SNAPPY\""
else
  . "$SH_SNAPPY"
  echo ". \"$SH_SNAPPY\""
fi
