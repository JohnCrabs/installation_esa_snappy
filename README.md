# installation_ESA_snappy
This project provides all the necessary components which are needed for installing ESA's **snappy**
python library. This library is important for Earth Observation development. Personally, I struggled
to try to install and run the library. For this reason, I decided to create this repository, which
contains all the necessary tools for the installation of **snappy**.

## Getting started
Before diving in the installation process, I would like to explain some of the major problems
during the installation process of **snappy**. In addition, I will explain how this installation
documentation can be expanded across several Python and Java versions. This information is produced
along the trial and error process, and it may help both the developers of **snappy** and **jpy**
tools, as well as the general community.

### Dependencies
The major problem, while installing **snappy** is the multiple dependencies which are needed during
the installation process. Actually, the **snappy** installation process can follow two ways based on
the Python version. For example, **snappy** can be easily installed for Python 3.6, without compiling
any **jpy** version (if I understood correctly, the **jpy** wheel for Python 3.6 exists in the 
snappy-python directory).

By several tries and errors, I figured out that the following dependencies can be used for safely
installing **snappy** without problems in several Python versions.

- **JAVA (JDK or JRE) >= v1.8** (for compiling **jpy**)
- **Maven >= v3.6.x** (for compiling **jpy**)
- **jpy == v0.9.0** (this is strictly, otherwise importing **snappy** in Python 3.x will produce the following error: _**RuntimeError: jpy: internal error: static method not found: unwrapProxy(Ljava/lang/Object;)Lorg/jpy/PyObject;**_)
- **snappy < v9.0** (I couldn't find **snappy 9.0** for testing it and **snappy 10.0 (esa-snappy)** promises to solve the jpy compatibility errors)

For those who are going to use Ubuntu in a Docker Environment (like me), it can help to install as well:
- build-essential
- software-properties-common
- wget
- git
- curl
- nano
- locate

### Similar documentations / Q&A forums
This documentation provides the basic steps for installing **snappy**. However, during my research
I found several sources, which helped me understand these tools. The following list summarises the
sources which I used during the installation process:

- Snappy where to start (**forum**): https://forum.step.esa.int/t/snappy-where-to-start/1463
- Problem with snappy and error (**forum**): https://forum.step.esa.int/t/problem-with-snappy-and-error/13606?page=3
- Esa-snappy (**repository**): https://github.com/senbox-org/esa-snappy
- Using SNAP in your Python programs (**documentation**): https://senbox.atlassian.net/wiki/spaces/SNAP/pages/24051781/Using+SNAP+in+your+Python+programs
- Problems with snappy and Python 3.8 (**forum**): https://forum.step.esa.int/t/problems-with-snappy-and-python-3-8/36833
- jpy-consortium/jpy (**repository**): https://github.com/jpy-consortium/jpy
- bcdev/jpy (**repository**): https://github.com/bcdev/jpy
- Install java and maven in windows 11 (**documentation**): https://community.jaspersoft.com/blog/install-java-and-maven-windows-11

## Contribution
This documentation repository contributes by summarizing the installation process of **snappy**,
providing appropriate commands and tips for the installation process. Thus, it can save time for
many developers like me, who struggling during the installation process of **snappy**.

## Installation

In this section, I describe the installation process of **snappy** library. Note that for
development purposes, this process can be replicated inside a Docker machine. I have successfully
tested the installation of **snappy** inside an Ubuntu-20.04 Docker environment. For this reason,
I will also add the commands for setting up the Docker environment. Furthermore, I will add the
commands for downloading alternative JDK, Maven, and Python versions for those who like to 
experiment or needed.

### Setting up an Ubuntu-20.04 Docker Environment for snappy installation

In this section, I will describe how to set up a Docker environment based on Ubuntu-20.04 for 
installing **snappy**. The installation process of **snappy** follows the steps of the next
section, thus only the prerequisites steps will be described. In addition, I assume that you
have already installed Docker on your machine. If not, use the instructions from the Docker
page https://www.docker.com/products/docker-desktop/ based on your machine. After the 
installation, you can follow the following steps:

1) Pull the Ubuntu-20.04 Docker Image:
```
docker pull ubuntu:20.04
```

2) Create and Name the container (use -it for preventing docker from exiting the container):
```
docker run --name <container-name> -it ubuntu:20.04
```

3) Start the container (optional if you used -it in the previous command):
```
docker start -i <container-name>
```

4) Install Ubuntu packages:
```
apt-get update && apt-get install build-essential software-properties-common wget git nano curl locate
```

5) Install Python3.x version:
```
add-apt-repository ppa:deadsnakes/ppa -y
apt-get update && apt-get install python3.x python3.x-dev python3.x-distutils gcc
```
6) Install Python pip:
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.x get-pip.py
```

#### Install in Docker (tested on Ubuntu:20.04)

For Docker installation, you can either use the commands provided in next 
section, or execute the following command:
```
apt-get update && apt-get install git -y && git clone https://gitlab.com/JohnCrabs/installation_esa_snappy.git && cd installation_esa_snappy && . setup.sh
```

Note that you need to execute the aforementioned command after running the Docker, without installing any other package.

### Unix Based Machines (Linux, Mac)
As previously mentioned, the **snappy** library can be installed easily by
using Python 3.6. In this section, we follow through the steps for installing
**snappy** for several Python versions, and I will pinpoint how this process
can be fast-forwarded in case of Python 3.6 usage. So, in case you are
using Python 3.6, please read carefully the following steps before applying
any unnecessary steps.

In the case, you would like to follow through the installation without using
this package I will provide the alternative commands of these steps.

1) Download this git package. As I previously mentioned, this package 
   contains all the necessary tools for the installation of snappy. 
```
git clone https://gitlab.com/JohnCrabs/installation_esa_snappy.git
```

2) Navigate through the JDK folder:
```
cd ./workstation/jdk/
```
Alternative:
```
mkdir workstation && cd workstation
mkdir jdk && cd jdk
wget <jdk-link> # jdk-link can be found at https://jdk.java.net/ 
                # by selecting and navigating to the JDK version 
                # you like and then right click + copy link 
                # through the download link.
```

3) Unzip the *.tar.gz file:
```
tar -xvf openjdk-*.tar.gz
```

4) Move the unzipped folder to system directory (optional):
```
mv jdk-* <new-jdk-path>
```

5) Add the following lines either **/root/.profile** or **~/.bashrc** by using nano or vim:
```
JAVA_HOME='<new-jdk-path>/jdk-*' # Specify the version in the *
PATH="$JAVA_HOME/bin:$PATH"
JDK_HOME=$JAVA_HOME
export PATH
export JAVA_HOME
export JDK_HOME
```

6) Navigate through maven directory (I assume you are in the directory containing workstation):
```
cd ./workstation/maven
```
Alternative:
```
cd workstation
mkdir maven && cd maven
wget <maven-link> # maven-link can be found at 
                  # https://mirrors.estointernet.in/apache/maven/maven-3/
                  # by selecting and navigating to the Maven 
                  # version you like and then right click + copy link 
                  # through the download link.
```

7) Unzip the *.tar.gz file:
```
tar -xvf apache-maven-*.tar.gz
```

8) Move the unzipped folder to system directory (optional):
```
mv apache-maven-* <new-apache-maven-path>
```

9) Add the following lines either **/root/.profile** or **~/.bashrc** by using nano or vim:
```
M2_HOME='<new-apache-maven-path>/apache-maven-*'
PATH="$M2_HOME/bin:$PATH"
export PATH
```

10) Source bash (use the file you added the lines):
```
source /root/.profile
```
or
```
source ~/.bashrc
```

11) Ensure that the installation of maven and JDK is correct:
```
java -version
maven -version
```

12) Navigate through jpy directory (I assume you are in the directory containing workstation):
```
cd ./workstation/jpy
```
Alternative:
```
cd workstation
mkdir jpy && cd jpy
wget https://github.com/bcdev/jpy/archive/refs/tags/0.9.0.tar.gz # Ensure that you downloaded the 0.9.0 version from this link
mv 0.9.0.tar.gz jpy-0.9.0.tar.gz
```

13) Unzip the *.tar.gz file and navigate to directory:
```
tar -xvf jpy-0.9.0.tar.gz
cd jpy-0.9.0
```

14) Compile jpy (This command will create a wheel for **jpy** in a new folder named **dist**):
```
python3.x setup.py build maven bdist_wheel # Remember to change x with the Python version.
```

15) Navigate to the snap folder and run execute the *.sh file (I assume you are in the directory containing workstation):
```
cd ./workstation/snap
sh esa-snap_all_unix_8_0.sh
```
Alternative:
```
cd workstation
mkdir snap && cd snap
wget https://step.esa.int/downloads/*.0/installers/esa-snap_all_unix_*_0.sh # Change the * with the version number you want. 
                                                                            # Acceptible values 5 <= * <= 8. sh esa-snap_all_unix_*_0.sh
```

16) Navigate to the location of snappy-conf file and execute it:
```
cd <path-to-snap-directory>/bin
./snappy-conf <path-to-python-version> # Usually the path to python version is 
                                       # **/usr/bin/python3.x** where x is the version.
```

Note that the previous step will fail if the python version is different from Python 3.6.
Actually, for Python 3.6 only the steps 15 and 16 are needed to be executed. The other steps
are required for the installation of **snappy** for python versions different from Python 3.6.

17) Move the wheel of jpy, which is created in step 14 to snappy directory (usually this path is at **/root/.snap/snap-python/snappy/**).
```
cp /workstation/jpy/dist/*.whl /root/.snap/snap-python/snappy/
```

18) Re-execute the snappy-conf command (step 16).
```
cd <path-to-snap-directory>/bin
./snappy-conf <path-to-python-version> # Usually the path to python version is 
                                       # **/usr/bin/python3.x** where x is the version.
```

If all goes well, the configuration will be finished successfully. If **jpy** version is
different from 0.9.0 the configuration will exit with code 30 due to **RuntimeError: jpy: 
internal error: static method not found: unwrapProxy(Ljava/lang/Object;)Lorg/jpy/PyObject;)**:

19) For importing the **snapy** library to python using **import snappy** use:
```
import sys
sys.path.append('/root/.snap/snap-python')  # This path may be differend in your machine.
                                            # you need to add the <path>/.snap/snap-python
```
Alternative (copy the **snappy** folder to **site-packages** folder of the Python3.x version or venv):
```
cp /root/.snap/snap-python/snappy /usr/local/lib/python3.8/site-packages
```

### Windows Based Machines

1) Download and Install Oracle Java by following the instructions from 
   https://docs.oracle.com/en/java/javase/11/install/installation-jdk-microsoft-windows-platforms.html#GUID-A7E27B90-A28D-4237-9383-A58B416071CA
   (Download the *.exe file and run it).
2) Add **JAVA_HOME** and **JDK_HOME** to **Environment Variables**.
3) Download and Install Maven by following the instruction from 
   https://maven.apache.org/download.cgi (Download the *.exe file and run it).
4) Add **MAVEN_HOME** to **Environment Variables**.
5) Download the jpy form https://github.com/bcdev/jpy/archive/refs/tags/0.9.0.zip
6) Extract it and open the directory.
7) Open a terminal inside the directory and execute the command:
```
# You need to have python in the Environment Variables as well for this command to work
python setup.py build maven bdist_wheel
```
8) Download and install ESA's SNAP All Toolbox from https://step.esa.int/main/download/snap-download/.
9) Navigate to **C:\Program Files\snap\bin** and open a terminal in this directory 
   (**right-click** and select **Open in Terminal**)
10) Execute the command (this will produce an error with code 10):
```
# Change the <use> with the user folder and * with Python version
.\snappy-conf.bat C:\Users\<user>\AppData\Local\Programs\Python\Python3*\python.exe  
```
11) Navigate to C:\Users\<user>\.snap\snap-python\snappy\snappy\ and copy the jpy wheel,
    which can be found in the dist folder created in jpy folder on step 7.
12) Repeat steps 9 and 10 (this will produce an error with code 30).
13) Navigate to C:\Users\<user>\.snap\snap-python\snappy\snappy\ and execute the command:
```
python setup.py build bdist_wheel  # This will produce a wheel which can be installed in a venv using pip
```
14) Navigate to the dist directory, open a terminal, open the venv (if any) and install the wheel using the pip command:
```
pip install *.whl
```

## Next Steps
I hope that you managed to install **jpy** and **snappy** following this documentation and without
a problem. Now it's time to start developing useful applications and packages to further help the
community in problem-solving :D. In the following list I provide some sources, which can help you
on starting the development using **snappy**.

- RUS Copernicus Training (**youtube channel**): https://www.youtube.com/@ruscopernicustraining5404
- How to use the SNAP API from Python (**documentation**): https://senbox.atlassian.net/wiki/spaces/SNAP/pages/19300362/How+to+use+the+SNAP+API+from+Python

## Authors and acknowledgment
This installation and documentation repository was based on the following 
packages, thus I would like to thank all the developers and communities 
for providing:
- OpenJDK under GPLv2 License (more info at https://openjdk.org/)
- Maven under Apache License (more info at https://maven.apache.org/)
- jpy under Apache License (more info at
  https://github.com/jpy-consortium/jpy and 
  https://github.com/bcdev/jpy)
- SNAP and snappy under GPLv3 Licence (more info at 
  https://step.esa.int/main/download/snap-download/ and 
  https://github.com/senbox-org/)

In addition, I would like to thank my colleagues for helping me along the 
process by providing both guidance and equipment when needed.

## License
This project is licensed under the GNU General Public License v3.0 or later.
